import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-breweries-common-table',
  templateUrl: './breweries-common-table.component.html',
  styleUrls: ['./breweries-common-table.component.scss']
})
export class BreweriesCommonTableComponent implements OnInit {
  @Input() data: any;
  @Input() page_name: any

  displayedColumns: string[] = ['id', 'name', 'brewery_type', 'street', 'city', 'state', 'country', 'longitude', 'latitude', 'postal_code'];

  constructor(
    private router: Router,
    private brewariesService: BreweriesService

  ) { }

  ngOnInit(): void {
   
  }

  city(city: any) {
    this.router.navigate(
      ['/brewaries/city'],
      { queryParams: { by_city: `${city}` } }
    );
  }

  dist(longitude: any, latitude: any) {
    this.router.navigate(
      ['/brewaries/dist'],
      { queryParams: { by_dist: `${longitude}, ${latitude}` } }
    );
  }

  name(name: any) {
    this.router.navigate(
      ['/brewaries/name'],
      { queryParams: { by_name: `${name}` } }
    );
  }

  state(state: any) {
    this.router.navigate(
      ['/brewaries/state'],
      { queryParams: { by_state: `${state}` } }
    );
  }

  postal(postal_code: any) {
    this.router.navigate(
      ['/brewaries/postal'],
      { queryParams: { by_postal: `${postal_code}` } }
    );
  }


  items: any = [
     'micro', 'nano', 'regional', 'brewpub', 'large', 'planning', 'bar', 'contract', 'proprietor', 'closed'
  ]

  onChange(event: any) {
    const by_type = event.source.value;
    this.brewariesService.getFilterbyType(by_type).subscribe((res: any) => {
      this.data = res
    });
  }

}
