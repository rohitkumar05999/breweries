import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreweriesCommonTableComponent } from './breweries-common-table.component';

describe('BreweriesCommonTableComponent', () => {
  let component: BreweriesCommonTableComponent;
  let fixture: ComponentFixture<BreweriesCommonTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreweriesCommonTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreweriesCommonTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
