import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreweriesCommonTableComponent } from './breweries-common-table/breweries-common-table.component';
import { MatTableModule } from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';




@NgModule({
  declarations: [
    BreweriesCommonTableComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    FlexLayoutModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
  ],
  exports: [BreweriesCommonTableComponent]
})
export class SharedModule { }
