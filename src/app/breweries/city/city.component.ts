import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  data: any;
  constructor(
    private route: ActivatedRoute,
    private brewariesService: BreweriesService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((res: any) => {
      this.brewariesService.getBreweriesByCity(res.by_city).subscribe((res: any) => {
        this.data = res;
        console.log('city', this.data)
      })
    })

  }

}
