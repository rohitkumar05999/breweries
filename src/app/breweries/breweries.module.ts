import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BreweriesRoutingModule } from './breweries-routing.module';
import { BreweriesComponent } from './breweries.component';
import { CityComponent } from './city/city.component';
import { SharedModule } from '../shared/shared.module';
import { DistComponent } from './dist/dist.component';
import { NameComponent } from './name/name.component';
import { StateComponent } from './state/state.component';
import { PostalComponent } from './postal/postal.component';


@NgModule({
  declarations: [
    BreweriesComponent,
    CityComponent,
    DistComponent,
    NameComponent,
    StateComponent,
    PostalComponent
  ],
  imports: [
    CommonModule,
    BreweriesRoutingModule,
    SharedModule
  ] 
})
export class BreweriesModule { }
