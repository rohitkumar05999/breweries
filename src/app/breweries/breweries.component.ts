import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BreweriesService } from '../services/breweries.service';

@Component({
  selector: 'app-breweries',
  templateUrl: './breweries.component.html',
  styleUrls: ['./breweries.component.scss']
})
export class BreweriesComponent implements OnInit {
  data: any
  displayedColumns: string[] = ['id', 'name', 'brewery_type', 'street', 'city', 'state', 'country', 'longitude', 'latitude'];

  constructor(
    private brewariesService: BreweriesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAllBreries();
  }


  getAllBreries() {
    this.brewariesService.getAllBreweries().subscribe((res: any) => {
      this.data = new MatTableDataSource();
      this.data = res;
      console.log("data", this.data)
    })
  }

 

}
