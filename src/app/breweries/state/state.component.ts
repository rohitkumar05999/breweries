import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})
export class StateComponent implements OnInit {

  data: any;

  constructor(
    private route: ActivatedRoute,
    private brewariesService: BreweriesService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((res: any) => {
      this.brewariesService.getBreweriesByState(res.by_state).subscribe((res: any) => {
        this.data = res;
        console.log('dist', this.data)
      })
    })
  }

}
