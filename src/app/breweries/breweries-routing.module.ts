import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreweriesComponent } from './breweries.component';
import { CityComponent } from './city/city.component';
import { DistComponent } from './dist/dist.component';
import { NameComponent } from './name/name.component';
import { PostalComponent } from './postal/postal.component';
import { StateComponent } from './state/state.component';

const routes: Routes = [
  {
    path: '',
    component: BreweriesComponent
  },
  {
    path: 'city',
    component: CityComponent
  },
  {
    path: 'dist',
    component: DistComponent
  },
  {
    path: 'name',
    component: NameComponent
  },
  {
    path: 'state',
    component: StateComponent
  },

  {
    path: 'postal',
    component: PostalComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BreweriesRoutingModule { }
