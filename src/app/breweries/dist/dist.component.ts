import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-dist',
  templateUrl: './dist.component.html',
  styleUrls: ['./dist.component.scss']
})
export class DistComponent implements OnInit {
  data: any;

  constructor(
    private route: ActivatedRoute,
    private brewariesService: BreweriesService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((res: any) => {
      this.brewariesService.getBreweriesByDist(res.by_dist).subscribe((res: any) => {
        this.data = res;
        console.log('dist', this.data)
      })
    })
  }

}
