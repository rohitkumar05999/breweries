import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-postal',
  templateUrl: './postal.component.html',
  styleUrls: ['./postal.component.scss']
})
export class PostalComponent implements OnInit {

  data: any;

  constructor(
    private route: ActivatedRoute,
    private brewariesService: BreweriesService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((res: any) => {
      this.brewariesService.getBreweriesByPostal(res.by_postal).subscribe((res: any) => {
        this.data = res;
        console.log('dist', this.data)
      })
    })
  }
}
