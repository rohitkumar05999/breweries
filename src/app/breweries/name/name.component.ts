import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreweriesService } from 'src/app/services/breweries.service';

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html',
  styleUrls: ['./name.component.scss']
})
export class NameComponent implements OnInit {

  data: any;

  constructor(
    private route: ActivatedRoute,
    private brewariesService: BreweriesService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((res: any) => {
      this.brewariesService.getBreweriesByName(res.by_name).subscribe((res: any) => {
        this.data = res;
        console.log('dist', this.data)
      })
    })
  }

}
