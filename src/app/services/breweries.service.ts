import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BreweriesService {

  private url = environment.baseUrl
  constructor(
    private http: HttpClient,
  ) { }


  getAllBreweries(): Observable<any> {
    return this.http.get(`${this.url}/breweries`);
  }


  getBreweriesByCity(by_city: any) {
    return this.http.get(`${this.url}/breweries?by_city=${by_city}`);
  }

  getBreweriesByDist(by_dist: any) {
    return this.http.get(`${this.url}/breweries?by_dist=${by_dist}`);
  }

  getBreweriesByName(by_name: any) {
    return this.http.get(`${this.url}/breweries?by_name=${by_name}`);
  }

  getBreweriesByState(by_state: any) {
    return this.http.get(`${this.url}/breweries?by_state=${by_state}`);
  }

  getBreweriesByPostal(by_postal: any) {
    return this.http.get(`${this.url}/breweries?by_postal=${by_postal}`);
  }

  getFilterbyType(by_type:any) {
    return this.http.get(`${this.url}/breweries?by_type=${by_type}`);
  }

}
